#[cfg(feature = "test-runner")]
mod async_writer_test {
    use clapcmd::{ArgMatches, ClapCmd, ClapCmdResult, Command};

    use std::io::Write;

    fn do_ping(cmd: &mut ClapCmd, _: ArgMatches) -> ClapCmdResult {
        let mut stdout = cmd.get_async_writer()?;
        std::thread::spawn(move || {
            std::thread::sleep(std::time::Duration::from_millis(10));
            write!(stdout, "pong later").ok();
        });
        Ok(())
    }

    #[test]
    fn async_output_logs_with_test_runner() {
        let mut cmd = ClapCmd::default();
        cmd.add_command(
            do_ping,
            Command::new("ping").about("do a ping after a countdown"),
        );
        let result = cmd.one_cmd("ping");
        if result.is_ok() {
            assert!(
                !cmd.async_output.lock().unwrap().contains("pong later"),
                "output arrived too early",
            );
            std::thread::sleep(std::time::Duration::from_millis(50));
            assert!(
                cmd.async_output.lock().unwrap().contains("pong later"),
                "output didn't arrive in time",
            );
            {
                cmd.async_output.lock().unwrap().clear();
            }
            std::thread::sleep(std::time::Duration::from_millis(10));
            {
                assert!(
                    !cmd.async_output.lock().unwrap().contains("pong later"),
                    "output arrived multiple times",
                );
            }
        } else {
            // this is an xfail if our test is run without a tty
            let result = result.unwrap_err();
            if result.to_string().contains("NOTTY") {
                println!("could not run rustyline due to lack of tty");
            } else {
                panic!("some unknown error happened");
            }
        }
    }
}
