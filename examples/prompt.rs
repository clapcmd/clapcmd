use clapcmd::{ArgMatches, ClapCmd, ClapCmdResult, Command};

#[derive(Clone)]
struct State {
    counter: u32,
}

fn do_count(cmd: &mut ClapCmd<State>, _: ArgMatches) -> ClapCmdResult {
    let state = cmd.get_state().ok_or("state missing")?;
    let new_count = state.counter + 1;
    cmd.info("adding one");
    cmd.set_state(State { counter: new_count });
    cmd.set_prompt(&format!("({})> ", new_count));
    Ok(())
}

fn main() {
    let mut cmd = ClapCmd::with_state(State { counter: 0 });
    cmd.set_prompt("(0)> ");
    cmd.add_command(
        do_count,
        Command::new("count").about("increment the prompt counter"),
    );
    cmd.run_loop();
}
