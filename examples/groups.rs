use clapcmd::{ArgMatches, ClapCmd, ClapCmdResult, Command, HandlerGroup};
use once_cell::sync::Lazy;

static LOADED_GROUP: Lazy<HandlerGroup> = Lazy::new(|| {
    ClapCmd::group("Fruit")
        .description("Commands to do cool fruit things")
        .command(
            do_apple,
            Command::new("apple").about("do the cool apple thing"),
        )
        .command(
            do_banana,
            Command::new("banana").about("do the cool banana thing"),
        )
        .command(
            do_unload,
            Command::new("unload").about("unload the cool fruit group"),
        )
});

static UNLOADED_GROUP: Lazy<HandlerGroup> = Lazy::new(|| {
    ClapCmd::unnamed_group().command(
        do_load,
        Command::new("load").about("load the cool fruit group"),
    )
});

fn do_load(cmd: &mut ClapCmd, _: ArgMatches) -> ClapCmdResult {
    cmd.add_group(&LOADED_GROUP);
    cmd.remove_group(&UNLOADED_GROUP);
    cmd.info("loaded");
    Ok(())
}

fn do_unload(cmd: &mut ClapCmd, _: ArgMatches) -> ClapCmdResult {
    cmd.add_group(&UNLOADED_GROUP);
    cmd.remove_group(&LOADED_GROUP);
    cmd.info("unloaded");
    Ok(())
}

fn do_apple(cmd: &mut ClapCmd, _: ArgMatches) -> ClapCmdResult {
    cmd.output("apple");
    Ok(())
}

fn do_banana(cmd: &mut ClapCmd, _: ArgMatches) -> ClapCmdResult {
    cmd.output("banana");
    Ok(())
}

fn main() {
    let mut cmd = ClapCmd::default();
    cmd.add_group(&UNLOADED_GROUP);
    cmd.run_loop();
}
