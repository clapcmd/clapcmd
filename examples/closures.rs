use clapcmd::{Arg, ArgMatches, ClapCmd, Command};

fn main() {
    let mut cmd = ClapCmd::default();
    cmd.add_command(
        |cmd: &mut ClapCmd<()>, _| {
            cmd.output("hello world");
            Ok(())
        },
        Command::new("hello").about("basic hello world"),
    );
    cmd.add_command(
        |cmd: &mut ClapCmd<()>, matches: ArgMatches| {
            cmd.output(format!(
                "hello, {}",
                matches.get_one::<String>("name").ok_or("missing option")?
            ));
            Ok(())
        },
        Command::new("greet").about("greet by name").arg(
            Arg::new("name")
                .help("name of person to greet")
                .required(true),
        ),
    );
    cmd.run_loop();
}
