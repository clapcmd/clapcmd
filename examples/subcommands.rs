use clapcmd::{Arg, ArgMatches, ClapCmd, ClapCmdResult, Command};

fn do_value(cmd: &mut ClapCmd, matches: ArgMatches) -> ClapCmdResult {
    cmd.output(format!("{:?}", matches));
    Ok(())
}

fn main() {
    let mut cmd = ClapCmd::default();
    cmd.add_command(
        do_value,
        Command::new("store")
            .about("keep track of values")
            .subcommand(
                Command::new("add")
                    .about("add a value")
                    .arg(Arg::new("value").help("value to add")),
            )
            .subcommand(
                Command::new("remove")
                    .about("remove a value")
                    .arg(Arg::new("value").help("value to remove")),
            ),
    );
    cmd.run_loop();
}
