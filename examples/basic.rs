use clapcmd::{ArgMatches, ClapCmd, ClapCmdResult, Command};

fn do_ping(cmd: &mut ClapCmd, _: ArgMatches) -> ClapCmdResult {
    cmd.output("pong");
    Ok(())
}

fn main() {
    let mut cmd = ClapCmd::default();
    cmd.add_command(do_ping, Command::new("ping").about("do a ping"));
    cmd.run_loop();
}
