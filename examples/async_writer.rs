use clapcmd::{ArgMatches, ClapCmd, ClapCmdResult, Command};

use std::io::Write;

fn do_ping(cmd: &mut ClapCmd, _: ArgMatches) -> ClapCmdResult {
    let mut stdout = cmd.get_async_writer()?;
    std::thread::spawn(move || {
        std::thread::sleep(std::time::Duration::from_millis(10));
        for i in (1..=3).rev() {
            {
                write!(stdout, "{}", i).ok();
            }
            std::thread::sleep(std::time::Duration::from_secs(1));
        }
        {
            write!(stdout, "pong later").ok();
        }
    });
    Ok(())
}

fn main() {
    let mut cmd = ClapCmd::default();
    cmd.add_command(
        do_ping,
        Command::new("ping").about("do a ping after a countdown"),
    );
    cmd.run_loop();
}
