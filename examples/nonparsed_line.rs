use clapcmd::{Arg, ArgMatches, ClapCmd, ClapCmdResult, Command};

fn do_shell(cmd: &mut ClapCmd, matches: ArgMatches) -> ClapCmdResult {
    let args: Vec<_> = matches
        .get_many::<String>("command")
        .unwrap_or_default()
        .collect();
    if args.len() > 0 {
        let output = std::process::Command::new(args[0])
            .args(&args[1..])
            .output();
        match output {
            Ok(output) => {
                cmd.output(format!(
                    "{}",
                    std::str::from_utf8(&output.stdout).unwrap_or_default()
                ));
            }
            Err(err) => {
                cmd.error(format!("ERROR: {}", err));
            }
        }
    }
    Ok(())
}

fn main() {
    let mut cmd = ClapCmd::default();
    cmd.add_command(
        do_shell,
        Command::new("shell").about("execute command in shell").arg(
            Arg::new("command")
                .help("the command to run")
                .num_args(1..)
                .trailing_var_arg(true)
                .allow_hyphen_values(true),
        ),
    );
    cmd.run_loop();
}
