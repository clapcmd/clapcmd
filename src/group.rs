use crate::handler::Handler;
use crate::Callback;
use crate::Command;

#[derive(Clone, Default, Hash, Eq, PartialEq)]
pub struct HandlerGroupMeta {
    pub name: String,
    pub description: String,
    pub visible: bool,
}

#[derive(Clone)]
pub struct HandlerGroup<State = ()> {
    pub(crate) group: HandlerGroupMeta,
    pub(crate) groups: Vec<Handler<State>>,
}

impl<State> HandlerGroup<State>
where
    State: Default,
{
    pub fn description(mut self, description: &str) -> Self {
        self.group.description = description.to_owned();
        self
    }

    pub fn command(mut self, callback: impl Callback<State>, command: Command) -> Self
    where
        State: Clone,
    {
        self.groups.push(Handler {
            command,
            group: Some(self.group.clone()),
            callback: callback.clone_box(),
        });
        self
    }
}
