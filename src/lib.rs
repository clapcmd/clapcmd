#![cfg_attr(all(doc, CHANNEL_NIGHTLY), feature(doc_auto_cfg))]
#![doc = include_str!("../README.md")]

/// convenience re-export of the clap crate
pub use clap;

/// convenience re-export of the rustyline crate
pub use rustyline;

pub use clap::{Arg, ArgMatches, Command, ValueHint};

mod handler;
pub use handler::{Callback, ClapCmdResult};

mod async_stdout;

mod helper;

mod group;
pub use group::HandlerGroup;

mod builder;
pub use builder::ClapCmdBuilder;

mod shell_parser;

mod cmd;
pub use cmd::ClapCmd;

pub mod errors;
