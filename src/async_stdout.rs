use std::io::Write;
#[cfg(feature = "test-runner")]
use std::sync::{Arc, Mutex};

pub use rustyline::ExternalPrinter;

pub struct AsyncStdout<T: ExternalPrinter> {
    pub(crate) printer: T,
    #[cfg(feature = "test-runner")]
    pub output: Arc<Mutex<String>>,
}

impl<T: ExternalPrinter> Write for AsyncStdout<T> {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        let s = std::str::from_utf8(buf);
        let s = match s {
            Ok(s) => String::from(s),
            Err(e) => {
                return Err(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    e.to_string(),
                ));
            }
        };
        #[cfg(feature = "test-runner")]
        {
            let s = s.clone();
            self.output.lock().unwrap().push_str(&s);
        }
        let res = self.printer.print(s);
        match res {
            Ok(_) => Ok(buf.len()),
            Err(e) => Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                e.to_string(),
            )),
        }
    }

    fn flush(&mut self) -> std::io::Result<()> {
        Ok(())
    }
}
